import anhDevcamp from "./assets/images/devcampreact.png";
import anhLogoReact from "./assets/images/logo-og.png";
import * as gInfo from "./info";

function App() {
    return (
        <div>
            <h1>{gInfo.gDevcampReact.title}</h1>
            <img src = {anhLogoReact} width = "1000" alt = "Ảnh react"/>
            <p>
                Tỷ lệ sinh viên đang theo học: {gInfo.calculatePercentOfStudying()}%
                <br/>
                {gInfo.calculatePercentOfStudying()>=15?"Sinh viên đăng ký học nhiều": "Sinh viên đăng ký học ít"}
            </p>
            <ul>
                {gInfo.gDevcampReact.benefits.map(function(paramValue, paramIndex){
                    return <li id = {paramIndex}>{paramValue}</li>
                })}
            </ul>
            <img src = {anhDevcamp} alt = "Ảnh devcamp" width = "1000"/>
        </div>
    );
}

export default App;
